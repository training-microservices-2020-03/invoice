package com.artivisi.training.microservices.invoice;

import com.artivisi.training.microservices.invoice.dao.InvoiceDao;
import com.artivisi.training.microservices.invoice.entity.Invoice;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@SpringBootTest
@Sql(
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = {"classpath:/sql/delete-data-invoice.sql",
                "classpath:/sql/sample-data-invoice.sql"})
public class InvoiceTests {

    private static final String SQL_HITUNG_INVOICE
            = "select count(id) from invoice where sales_reference='S-001'";

    @Autowired private JdbcTemplate jdbcTemplate;
    @Autowired private InvoiceDao invoiceDao;

    @BeforeAll
    public static void inisialisasiClass() {
        System.out.println("Ini dijalankan sekali sebelum semua test");
    }

    @AfterAll
    public static void cleanupClass() {
        System.out.println("Ini jalan sekali saja setelah semua test");
    }

    @BeforeEach
    public void sebelumTiapTest() {
        System.out.println("Dijalankan sebelum masing-masing test");
    }

    @AfterEach
    public void setelahTiapTest() {
        System.out.println("Dijalankan setelah masing-masing test");
    }

    @Test
    public void testCreateInvoice() {
        System.out.println("Test membuat invoice");

        Invoice inv = new Invoice();
        inv.setAmount(new BigDecimal(100000));
        inv.setSalesReference("S-001");
        inv.setTransactionTime(LocalDateTime.now());
        inv.setCustomerName("Customer 001");
        inv.setCustomerEmail("customer001@yopmail.com");
        inv.setDescription("Test create invoice");

        Integer jumlahRecordSebelumInsert =
                jdbcTemplate.queryForObject(SQL_HITUNG_INVOICE, Integer.class);
        Assertions.assertEquals(0, jumlahRecordSebelumInsert);

        invoiceDao.save(inv);

        Integer jumlahRecordSetelahInsert =
                jdbcTemplate.queryForObject(SQL_HITUNG_INVOICE, Integer.class);
        Assertions.assertEquals(1, jumlahRecordSetelahInsert);
    }

    @Test
    public void testListInvoice() {
        System.out.println("Test melihat isi tabel invoice");

        Page<Invoice> dataInvoice = invoiceDao.findAll(Pageable.unpaged());
        Assertions.assertEquals(4, dataInvoice.getTotalElements());
        Assertions.assertEquals(1, dataInvoice.getTotalPages());

        Invoice i = dataInvoice.getContent().get(0);
        Assertions.assertEquals(new BigDecimal("100001.00"), i.getAmount());
        Assertions.assertEquals("S-901", i.getSalesReference());
    }
}
