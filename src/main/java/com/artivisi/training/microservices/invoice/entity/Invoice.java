package com.artivisi.training.microservices.invoice.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity @Data
public class Invoice {

    @Id
    @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;
    private String salesReference;
    private LocalDateTime transactionTime;
    private String customerName;
    private String customerEmail;
    private String description;
    private BigDecimal amount;
}
