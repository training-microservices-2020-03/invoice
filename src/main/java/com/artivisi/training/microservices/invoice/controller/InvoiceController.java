package com.artivisi.training.microservices.invoice.controller;

import com.artivisi.training.microservices.invoice.dao.InvoiceDao;
import com.artivisi.training.microservices.invoice.entity.Invoice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController @RequestMapping("/api/invoice")
public class InvoiceController {

    @Autowired private InvoiceDao invoiceDao;

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Invoice invoice) {

        invoiceDao.save(invoice);
        log.info("Invoice {} created for sale {}",
                invoice.getId(),
                invoice.getSalesReference());
    }

    @GetMapping("/")
    public Page<Invoice> findAll(Pageable pageable) {
        return invoiceDao.findAll(pageable);
    }

    @GetMapping("/hostinfo")
    public Map<String, Object> hostinfo(HttpServletRequest request) {
        Map<String, Object> info = new HashMap<>();
        info.put("address", request.getLocalAddr());
        info.put("port", request.getLocalPort());
        log.info("Invoice service info : {}", info.toString());
        return info;
    }
}
