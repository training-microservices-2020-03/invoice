package com.artivisi.training.microservices.invoice.dao;

import com.artivisi.training.microservices.invoice.entity.Invoice;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InvoiceDao extends PagingAndSortingRepository<Invoice, String> {
}
